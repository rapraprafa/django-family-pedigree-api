from django.db import models
from django.contrib.auth.models import User, auth

# Create your models here.

class Family(models.Model):
    familylistowner_id = models.ForeignKey(User, on_delete=models.CASCADE)
    relations = models.TextField()
    firstnames = models.TextField()
    lastnames = models.TextField()
    birthdays = models.TextField()
    vitalstatuses = models.TextField()