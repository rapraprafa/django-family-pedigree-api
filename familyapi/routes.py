from django.urls import path
from django.conf.urls import url
from .views import FamilyAPIView, FamilyAPIWithID, UserViewSet, UserLogInView
from oauth2_provider import views as oauth2_views

urlpatterns = [
    path('login/', UserLogInView.as_view()),
    path('family/', FamilyAPIView.as_view()),
    path('family/<int:userid>', FamilyAPIWithID.as_view())
]