from django.apps import AppConfig


class FamilyapiConfig(AppConfig):
    name = 'familyapi'
