from django.shortcuts import render
from .models import Family
from .serializers import FamilySerializer, UserSerializer, LoginSerializer
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.response import Response
from django.contrib.auth.models import User, auth
from django.http import HttpResponse
from rest_framework import status
from rest_framework.permissions import AllowAny
from oauth2_provider.settings import oauth2_settings
from django.db import transaction
from django.db.models import Q
from oauth2_provider.views.mixins import OAuthLibMixin
from braces.views import CsrfExemptMixin
import json
import operator
from functools import reduce


# Create your views here.
class UserLogInView(APIView):      
    def post(self, request):
        data = request.data
        serializer = LoginSerializer(data=data)
        if serializer.is_valid():
            user = auth.authenticate(username=request.data.get('username'), password=request.data.get('password'))
            if user is not None:
                auth.login(request, user)
                return Response (serializer.data, status=status.HTTP_200_OK)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def update(self, request, *args, **kwargs):
        partial = True # Here I change partial to True
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)


class FamilyAPIView(APIView):
    def get(self, request):
        families = Family.objects.all()
        serializer = FamilySerializer(families, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = FamilySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

class FamilyAPIWithID(APIView):
    def get_object(self, userid):
        try:
            return Family.objects.get(familylistowner_id=userid)

        except Family.DoesNotExist:
            return HttpResponse(status=404)
    
    def get(self, request, userid):
        family = self.get_object(userid)
        serializer = FamilySerializer(family)
        return Response(serializer.data)
    
    def put(self, request, userid):
        family = self.get_object(userid)
        serializer = FamilySerializer(family, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)