from rest_framework import serializers
from .models import Family
from django.contrib.auth.models import User, auth
from django.db.models import TextField

class FamilySerializer(serializers.ModelSerializer):
    class Meta:
        model = Family
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','username','first_name','last_name','email','password']
    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance


class LoginSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    username = serializers.CharField(max_length=100)
    password = serializers.CharField(max_length=100)
    first_name = serializers.CharField(max_length=100, required=False)
    last_name = serializers.CharField(max_length=100, required=False)
    email = serializers.CharField(max_length=100, required=False)

    def validate(self, request):
        try:
            user = auth.authenticate(username=request.get('username'), password=request.get('password'))
            print(user)
            if user is None:
                raise serializers.ValidationError(("Invalid credentials"))
        except User.DoesNotExist:
            pass
            user = User.objects.get()
        user = auth.authenticate(username=request.get('username'), password=request.get('password'))
        return user